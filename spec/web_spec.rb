# frozen_string_literal: true

RSpec.describe Anagrams::Web do
  include Rack::Test::Methods

  def app
    described_class
  end

  it 'returns anagrams in JSON' do
    get '/crepitus'
    expect(last_response).to be_ok
    expect(last_response.body).to eq(
      Oj.dump('crepitus' => %w[cuprites pictures piecrust]))
    get '/crepitus,paste,kinship,enlist,boaster,fresher,sinks,knits,sort'
    expect(last_response).to be_ok
    expect(last_response.body).to eq(
      Oj.dump('crepitus' => %w[cuprites pictures piecrust],
              'paste' => %w[pates peats septa spate tapes tepas],
              'kinship' => %w[pinkish],
              'enlist' => %w[elints inlets listen silent slinte tinsel],
              'boaster' => %w[barotse boaters borates rebatos sorbate],
              'fresher' => %w[refresh],
              'sinks' => %w[skins],
              'knits' => %w[skint stink tinks],
              'sort' => %w[orts rots stor tors]))
  end

  it 'returns an empty array if lookup fails' do
    get '/abcdefghijklmno'
    expect(last_response).to be_ok
    expect(last_response.body).to eq(Oj.dump('abcdefghijklmno' => []))
  end
end
