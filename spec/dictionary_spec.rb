# frozen_string_literal: true

RSpec.describe Anagrams::Dictionary do
  context 'keyification' do
    it 'works as expected' do
      expect(described_class.keyify('auditorium')).to eq('adiimortuu')
      expect(described_class.keyify('CUCUMBER')).to eq('BCCEMRUU')
      expect(described_class.keyify("glaciation's")).to eq("'aacgiilnost")
    end
  end

  context 'importing' do
    let(:file) { File.join(File.dirname(__FILE__), 'fixtures', 'wordlist.txt') }
    let(:dict) { described_class.new(source: file) }

    it 'imports all keys' do
      expect(File.read(file).scan(/\n/).count).to eq(39)
      expect(dict.keys.count).to eq(9)
    end
  end
end
