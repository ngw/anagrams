# frozen_string_literal: true

RSpec.describe Anagrams::SolvingService do
  let(:file) { File.join(File.dirname(__FILE__), 'fixtures', 'wordlist.txt') }
  let(:dict) { Anagrams::Dictionary.new(source: file) }

  it 'returns anagrams for multiple wordlist' do
    service = described_class.new(querystrings: %w[crepitus sort], store: dict)
    expect(service.perform).to eq({ 'crepitus' => %w[cuprites pictures piecrust],
                                    'sort' => %w[orts rots stor tors] })
  end
end
