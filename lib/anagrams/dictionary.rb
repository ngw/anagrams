# frozen_string_literal: true

module Anagrams
  class Dictionary
    extend Forwardable
    def_delegators :@_data, :[], :keys

    def initialize(source:)
      @_data = Hash.new { |hash, key| hash[key] = [] }
      parse(File.open(source, 'r'))
    end

    def self.keyify(string)
      string.chars.sort.join
    end

    private

    def parse(source)
      source.each_line do |line|
        word = line.strip.downcase.encode(
          'UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
        @_data[Dictionary.keyify(word)] << word
      end
    end
  end
end
