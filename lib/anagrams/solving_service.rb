# frozen_string_literal: true

module Anagrams
  class SolvingService
    def initialize(querystrings:, store:)
      @querystrings = querystrings
      @store = store
    end

    def perform
      results = {}
      @querystrings.each do |query|
        results[query] = @store[Dictionary.keyify(query)].reject do |elem|
          elem == query
        end
      end
      results
    end
  end
end
