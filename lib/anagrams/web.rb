# frozen_string_literal: true

module Anagrams
  class Web < Sinatra::Base
    configure do
      set :root, File.expand_path(File.join(File.dirname(__FILE__), '/../..'))
      set :file, File.join(settings.root, 'assets/wordlist.txt')
      set :store, Dictionary.new(source: file)
    end

    get '/:anagrams' do
      querystrings = params[:anagrams].split(',').map(&:strip).map(&:downcase)
      solver = SolvingService.new(querystrings: querystrings, store: settings.store)
      Oj.dump(solver.perform)
    end
  end
end
