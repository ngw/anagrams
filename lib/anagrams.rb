# frozen_string_literal: true

require 'forwardable'
require 'oj'
require 'sinatra'

require_relative 'anagrams/dictionary'
require_relative 'anagrams/solving_service'
require_relative 'anagrams/web'

module Anagrams
  VERSION = '0.1'
end
