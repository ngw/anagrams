```
ngw@ohvelveteen ~/Projects/anagrams: bundle exec rackup

ngw@ohvelveteen ~/: ab -n 100 http://localhost:9292/crepitus,paste,kinship,enlist,boaster,fresher,sinks,knits,sort
This is ApacheBench, Version 2.3 <$Revision: 1826891 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        WEBrick/1.4.2
Server Hostname:        localhost
Server Port:            9292

Document Path:          /crepitus,paste,kinship,enlist,boaster,fresher,sinks,knits,sort
Document Length:        365 bytes

Concurrency Level:      1
Time taken for tests:   0.177 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      64300 bytes
HTML transferred:       36500 bytes
Requests per second:    564.73 [#/sec] (mean)
Time per request:       1.771 [ms] (mean)
Time per request:       1.771 [ms] (mean, across all concurrent requests)
Transfer rate:          354.61 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       1
Processing:     1    2   2.8      1      28
Waiting:        1    2   2.8      1      28
Total:          1    2   2.8      1      28

Percentage of the requests served within a certain time (ms)
  50%      1
  66%      1
  75%      1
  80%      2
  90%      2
  95%      2
  98%      9
  99%     28
 100%     28 (longest request)
```